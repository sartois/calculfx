package me.artois;

import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import com.udojava.evalex.Expression;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public class PrimaryController implements Initializable {

    /**
     * The main string property where all math is added
     */
    private StringProperty toCompute = new SimpleStringProperty("");

    /**
     * Used to handle , and . as separator
     */
    private DecimalFormat df = new DecimalFormat();

    /**
     * After clicking =, we expect to remove displayed result when we click
     * somewhere else
     */
    private boolean flagReplace = false;

    public final StringProperty toComputeProperty() {
        return toCompute;
    }

    public final String getToCompute() {
        return toCompute.get();
    }

    public final void setToCompute(String value) {
        toCompute.set(value);
    }

    public final void appendToCompute(String value) {
        setToCompute((new StringBuilder()).append(getToCompute()).append(value).toString());
    }

    /**
     * Collect every math computed lines
     */
    protected List<String> historyData = new ArrayList<>();

    /**
     * Used to bind history data with view
     */
    protected ListProperty<String> historyProperty = new SimpleListProperty<>();

    /**
     * Ref to the history ListView
     */
    @FXML
    private ListView<String> historyView;

    /**
     * Ref to the main text field where we add math operator
     */
    @FXML
    private TextField computer;

    /**
     * Event listener where button text is added to toCompute
     */
    @FXML
    private void handleSimpleButtonAction(ActionEvent event) {
        String text = ((Button) event.getSource()).getText();
        if (flagReplace) {
            flagReplace = false;
            setToCompute(text);
        } else {
            appendToCompute(text);
        }
    }

    /**
     * Event listener for "="
     */
    @FXML
    private void compute(ActionEvent event) {
        String current = getToCompute();

        // Handle "divide by zero"
        if (current.endsWith("/0")) {
            setToCompute(current.replace("/0", "/"));
            return;
        }

        /**
         * @see https://github.com/uklimaschewski/EvalEx
         */
        Expression expression = new Expression(current);
        BigDecimal result = expression.eval();

        String resultAsString = df.format(result);
        setToCompute(resultAsString);

        historyData.add(current + " = " + resultAsString);

        historyProperty.set(FXCollections.observableArrayList(historyData));

        flagReplace = true;
    }

    /**
     * Event listener for "c"
     */
    @FXML
    private void erase(ActionEvent event) {
        String current = getToCompute();
        if (current.length() > 0) {
            current = current.substring(0, current.length() - 1);
            setToCompute(current);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        historyView.itemsProperty().bind(historyProperty);

        Locale currentLocale = Locale.getDefault();
        DecimalFormatSymbols mathReadySymbols = new DecimalFormatSymbols(currentLocale);
        mathReadySymbols.setDecimalSeparator('.');
        mathReadySymbols.setGroupingSeparator(' ');

        df.setMaximumFractionDigits(5);
        df.setMinimumFractionDigits(0);
        df.setGroupingUsed(false);
        df.setDecimalFormatSymbols(mathReadySymbols);

        // Allow textfield direct edit, AKA, with the keyboard
        computer.textProperty().bindBidirectional(toComputeProperty());
    }
}
