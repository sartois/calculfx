Simple JavaFX calculator

Practical work for [MUX-102](http://formation.cnam.fr/rechercher-par-discipline/interaction-humain-machine-conception-d-interfaces-et-experience-utilisateur-1085794.kjsp) @ [Cnam Paris](http://www.cnam-paris.fr/cnam-paris/300-parcours-de-formation-de-bac-a-bac-8-a-paris-accueil-1053653.kjsp)

Build and run with [Maven](https://maven.apache.org/)

```
> mvn install

> mvn exec:java
```
